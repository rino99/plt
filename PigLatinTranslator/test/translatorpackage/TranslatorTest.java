package translatorpackage;

import static org.junit.Assert.*;
import org.junit.Test;

public class TranslatorTest {
	
	private static final String PHRASE_TEST = "hello world";
	
	@Test
	public void testInputPhrase() {
		String inputPhrase =  PHRASE_TEST;
		Translator translator = new Translator(inputPhrase);
		assertEquals(PHRASE_TEST, translator.getPhrase());
	}
	
	@Test
	public void testTranslate() throws PigLatinException {
		String inputPhrase =  "";
		Translator translator = new Translator(inputPhrase);
		assertEquals(translator.NIL, translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStringWithVowelEndigWithY() throws PigLatinException {
		String inputPhrase =  "any";
		Translator translator = new Translator(inputPhrase);
		assertEquals("anynay", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStringWithUEndigWithY() throws PigLatinException {
		String inputPhrase =  "utility";
		Translator translator = new Translator(inputPhrase);
		assertEquals("utilitynay", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStringWithVowelEndigWithVowel() throws PigLatinException {
		String inputPhrase =  "apple";
		Translator translator = new Translator(inputPhrase);
		assertEquals("appleyay", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStringWithVowelEndigWithConsonant() throws PigLatinException {
		String inputPhrase =  "ask";
		Translator translator = new Translator(inputPhrase);
		assertEquals("askay", translator.translate());
	}

	@Test
	public void testTranslationPhraseStringWithConsonant() throws PigLatinException {
		String inputPhrase =  "hello";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStringWithSequenceConsonant() throws PigLatinException {
		String inputPhrase =  "known";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ownknay", translator.translate());
	}
	
	@Test
	public void testTranslationLongPhraseWithSpace() throws PigLatinException {
		String inputPhrase =  PHRASE_TEST;
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay orldway", translator.translate());
	}
	
	@Test
	public void testTranslationLongPhraseWithDash() throws PigLatinException {
		String inputPhrase =  "well-being";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellway-eingbay", translator.translate());
	}

	
	@Test
	public void testTranslationLongPhraseWithPunctuations() throws PigLatinException {
		String inputPhrase =  "hello world!";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay orldway!", translator.translate());
	}
	
    @Test
	public void testTranslationLongPhraseWithParenthesis() throws PigLatinException {
		String inputPhrase =  "(hello world)";
		Translator translator = new Translator(inputPhrase);
		assertEquals("(ellohay orldway)", translator.translate());
	}
    
    @Test
   	public void testTranslationLongPhraseWithaApostrophe() throws PigLatinException {
   		String inputPhrase =  "hello' world";
   		Translator translator = new Translator(inputPhrase);
   		assertEquals("ellohay' orldway", translator.translate());
   	}
    
    @Test(expected=PigLatinException.class)
	public void testTranslateErrorWhitDash() throws PigLatinException {
    	String inputPhrase =  "hello--world";
		Translator translator = new Translator(inputPhrase);
		translator.translate();
	}
    
    @Test(expected=PigLatinException.class)
	public void testTranslateErrorWithSpace() throws PigLatinException {
    	String inputPhrase =  "hello  world";
		Translator translator = new Translator(inputPhrase);
		translator.translate();
	}
    
}
