package translatorpackage;

public class Translator {
	
	private String phrase;
	public static final String NIL = "nil";
	private StringBuilder translation = new StringBuilder();
	
	public Translator(String inputPhrase) {
		this.phrase = inputPhrase;
	}

	public Object getPhrase() {
		return this.phrase;
	}

	public Object translate() throws PigLatinException {	
		phrase = phrase + '$';
		String phraseBackup = phrase;
		StringBuilder parser = new StringBuilder();
		int index = 0;
		int i = 0;

		while(i < phrase.length()) {
			if(shouldStopParserManipulation(i)) {	
				if(phrase.charAt(i) != '$') {
					checkError(i+1);
				}
				handleParsingAndTranslate(parser, phraseBackup, i,index);
				parser = new StringBuilder();
				phrase = phraseBackup.substring(index+1, phraseBackup.length());
				i = 0;
			}else {
				parser.append(phrase.charAt(i));	
				i++;
			}
			index++;
		}	

		return translation.deleteCharAt(translation.length()-1).toString();  
	}
	
	private boolean startWithVowel() {
		return (phrase.startsWith("a") || phrase.startsWith("e") || phrase.startsWith("i") || phrase.startsWith("o") || phrase.startsWith("u"));
	}

	private boolean endtWithVowel() {
		return (phrase.endsWith("a") || phrase.endsWith("e") || phrase.endsWith("i") || phrase.endsWith("o") || phrase.endsWith("u"));
	}
	
	private boolean endWithPunctuations() {
		return (phrase.endsWith(".") || phrase.endsWith(",") || phrase.endsWith(":") || phrase.endsWith(";") || phrase.endsWith("?") || phrase.endsWith("!") || phrase.endsWith("'") || phrase.endsWith("(") || phrase.endsWith(")"));
	}
	
	private boolean startWithPunctuations() {
		return (phrase.startsWith(".") || phrase.startsWith(",") || phrase.startsWith(":") || phrase.startsWith(";") || phrase.startsWith("?") || phrase.startsWith("!") || phrase.startsWith("'") || phrase.startsWith("(") || phrase.startsWith(")"));
	}
	
	private boolean isAVolwe(Character letter) {
		return (letter.equals('a') || letter.equals('e') || letter.equals('i') || letter.equals('o') || letter.equals('u'));
	}
	
	private boolean shouldStopParserManipulation(int index) {
		return (phrase.charAt(index) == ' ' || phrase.charAt(index) == '$' || phrase.charAt(index) == '-');
	}
	
	private void checkError(int index) throws PigLatinException {
		if(shouldStopParserManipulation(index)) {
			throw new PigLatinException("---Syntax error unexpected in phrase---");
		}
	}
	
	private void handleParsingAndTranslate(StringBuilder parser, String phraseBackup, int i, int index) {
		phrase = parser.toString();
		Character punctuationsInitial = null;
		Character punctuationsFinal = null;
		if(endWithPunctuations()) {
			punctuationsFinal = phrase.charAt(i-1);
			phrase = phrase.substring(0, i-1);
		}else if(startWithPunctuations()) {
			punctuationsInitial = phrase.charAt(0);
			phrase = phrase.substring(1, i);
			translation.append(punctuationsInitial);
		}
		
		translation.append(translateSingleWord());
		
		if(phraseBackup.charAt(index) == '-') {
			translation.append("-");
		}else if(punctuationsFinal != null){
			translation.append(punctuationsFinal);
			translation.append(" ");
		}else {
			translation.append(" ");
		}
	}
	
	private String translateSingleWord() {
		if(startWithVowel() ) {
			if(phrase.endsWith("y")) {
				return phrase + "nay";
			}else if(endtWithVowel()) {
				return phrase + "yay";
			}else if(!endtWithVowel()) {
				return phrase + "ay";
			}
		}
		if(!startWithVowel() && !phrase.equals("")){
			StringBuilder partPhrase = new StringBuilder();
			partPhrase.append(phrase.charAt(0));
			int counter = 1;	
			for(int i = 1; i < phrase.length(); i++) {	
				if(!isAVolwe(phrase.charAt(i))) {
					partPhrase.append(phrase.charAt(i));
					counter++;
				}else if(isAVolwe(phrase.charAt(i))) {
					break;
				}
			}
			return phrase.substring(counter, phrase.length()) + partPhrase + "ay";
		}	
		return NIL;
	}
}
